<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "player_games".
 *
 * @property integer $game_id
 * @property integer $player_id
 * @property integer $score
 * @property integer $result
 *
 * @property Games $game
 * @property Players $player
 */
class PlayerGames extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'player_games';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_id', 'player_id', 'score', 'result'], 'integer'],
            [['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => Games::className(), 'targetAttribute' => ['game_id' => 'id']],
            [['player_id'], 'exist', 'skipOnError' => true, 'targetClass' => Players::className(), 'targetAttribute' => ['player_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'game_id' => 'Game ID',
            'player_id' => 'Player ID',
            'score' => 'Score',
            'result' => 'Result',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Games::className(), ['id' => 'game_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayer()
    {
        return $this->hasOne(Players::className(), ['id' => 'player_id']);
    }

    public function getOpponent()
    {
        $opponentGameResult = $this->hasOne(Players::className(), ['id != :id',  ['id' => 'player_id']])->one();

        return $opponentGameResult->getPlayer();
    }
}
