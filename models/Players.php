<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "players".
 *
 * @property integer $id
 * @property string $firstName
 * @property string $lastName
 * @property string $email
 * @property string $phone
 * @property string $startDate
 * @property string $endDate
 *
 * @property PlayerGames[] $playerGames
 */
class Players extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'players';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstName', 'lastName'], 'required'],
            [['startDate', 'endDate'], 'safe'],
            [['firstName', 'lastName'], 'string', 'max' => 80],
            [['email'], 'string', 'max' => 120],
            [['phone'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstName' => 'First Name',
            'lastName' => 'Last Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'startDate' => 'Start Date',
            'endDate' => 'End Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerGames()
    {
        return $this->hasMany(PlayerGames::className(), ['player_id' => 'id']);
    }

    public function getFullName()
    {
        return $this['firstName'] . ' ' . $this['lastName'];
    }

    public static function getPlayersName()
    {
        $players = self::find()->all();

        $playersNameArray = ArrayHelper::map(
            $players,
            function($model) {
                return $model['id'];
            },
            function($model) {
                /** @var Players $model */
                return $model->getFullName();
            }
        );

        return $playersNameArray;
    }
}
