<?php

namespace app\models;

use Yii;

/**
 * .
 */
class PlayerStatistics
{
    /** @var Players $player */
    private $player;

    /** @var Games $highestScoreGame */
    private $highestScoreGame;

    /** @var boolean $highestScoreGameResult */
    private $highestScoreGameResult;

    /**
     * PlayerStatistics constructor.
     *
     * @param Players $player
     * @return PlayerStatistics
     */
    public function __construct($player)
    {
        $this->player = $player;

        /** @var PlayerGames $playerHighestScoreGame */
        $playerHighestScoreGame       = $this->getPlayerGames()->orderBy('score DESC')->one();
        $this->highestScoreGameResult = $playerHighestScoreGame['result'];
        $this->highestScoreGame       = $playerHighestScoreGame->getGame()->one();
    }

    /**
     *
     * @param bool $winner If $winner = true - return array of winning games, otherwise - losing games.
     * @return array
     */
    public function getGames($winner = true)
    {
        $games = $this->getPlayerGames()->where(['result' => $winner])->all();

        return $games;
    }

    public function getAverageScore()
    {
        return $this->getPlayerGames()->average('score');
    }

    public function getHighestScore()
    {
        return $this->getPlayerGames()->max('score');
    }

    public function getHighestScoreGameOpponentName()
    {
        $opponent = $this->getHighestScoreGame()->getPlayer(!$this->getHighestScoreGameResult());

        return $opponent->getFullName();
    }

    public function getHighestScoreGameResult()
    {
        return $this->highestScoreGameResult;
    }

    public function getHighestScoreGameId()
    {
        return $this->highestScoreGame['id'];
    }

    private function getHighestScoreGame()
    {
        return $this->highestScoreGame;
    }

    private function getPlayerGames()
    {
        return $this->player->getPlayerGames();
    }
}
