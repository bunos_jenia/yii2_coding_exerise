<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "games".
 *
 * @property integer $id
 * @property string $date
 *
 * @property PlayerGames[] $playerGames
 */
class Games extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'games';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['winnerScore', 'loserScore'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'score' => 'Score',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerGames()
    {
        return $this->hasMany(PlayerGames::className(), ['game_id' => 'id']);
    }

    /**
     * Method which return Players Object.
     *
     * @param bool $winner If winner = true - return winner, otherwise - loser.
     * @return Players
     */
    public function getPlayer($winner = true)
    {
        $playerGameObj = $this->getPlayerGames()->where(['result' => $winner])->one();

        return $playerGameObj->getPlayer()->one();
    }

    /**
     * Return score string
     *
     * @return string
     */
    public function getScore()
    {
        $playerGameObj = $this->getPlayerGames()->all();

        return implode(':', ArrayHelper::getColumn($playerGameObj, 'score'));
    }
}
