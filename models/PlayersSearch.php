<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Players;

/**
 * PlayersSearch represents the model behind the search form about `app\models\Players`.
 */
class PlayersSearch extends Players
{
    /** @const int LEADERBOARD_PLAYERS_COUNT */
    CONST LEADERBOARD_PLAYERS_COUNT = 10;

    /** @const int LEADERBOARD_MIN_PLAYED_GAMES */
    CONST LEADERBOARD_MIN_PLAYED_GAMES = 10;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['firstName', 'lastName', 'email', 'phone', 'startDate', 'endDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Players::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'startDate' => $this->startDate,
            'endDate' => $this->endDate,
        ]);

        $query->andFilterWhere(['like', 'firstName', $this->firstName])
            ->andFilterWhere(['like', 'lastName', $this->lastName])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance of players with the top 10 average scores and played at least 10 games.
     *
     * @return ActiveDataProvider
     */
    public function searchLeaders()
    {
        $leadersArray = PlayerGames::find()
            ->select('Avg(score) as average, player_id')
            ->joinWith('player')
            ->groupBy('player_id')
            ->having('COUNT(player_id) >= :gamesCount', ['gamesCount' => self::LEADERBOARD_MIN_PLAYED_GAMES])
            ->orderBy('Avg(score) DESC')
            ->limit(self::LEADERBOARD_PLAYERS_COUNT)
            ->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $leadersArray,
            'pagination' => false,
        ]);

        return $dataProvider;
    }
}
