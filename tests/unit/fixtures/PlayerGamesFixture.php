<?php

namespace app\tests\unit\fixtures;

use yii\test\ActiveFixture;

class PlayerGamesFixture extends ActiveFixture
{
    public $modelClass = \app\models\PlayerGames::class;
}
