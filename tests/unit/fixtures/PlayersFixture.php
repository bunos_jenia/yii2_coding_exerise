<?php

namespace app\tests\unit\fixtures;

use yii\test\ActiveFixture;

class PlayersFixture extends ActiveFixture
{
    public $modelClass = \app\models\Players::class;
}
