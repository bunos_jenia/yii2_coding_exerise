<?php

namespace app\tests\unit\fixtures;

use yii\test\ActiveFixture;

class GamesFixture extends ActiveFixture
{
    public $modelClass = \app\models\Games::class;
}
