<?php

$playerGamesFixtureArray = [];

for ($gameId = 1; $gameId < 300; $gameId++) {
    $winnerId = rand(1, 12);
    $loserId  = rand(1, 12);

    if ($winnerId == $loserId) {
        ($winnerId > 5) ? --$loserId : ++$winnerId;
    }

    $winnerScore = rand(21, 100);
    $loserScore  = $winnerScore - rand(1, 20);

    $playerGamesFixtureArray[] = [
        'game_id'   => $gameId,
        'player_id' => $winnerId,
        'score'     => $winnerScore,
        'result'    => true,
    ];
    $playerGamesFixtureArray[] = [
        'game_id'   => $gameId,
        'player_id' => $loserId,
        'score'     => $loserScore,
        'result'    => false,
    ];
}


return $playerGamesFixtureArray;
