<?php

$gamesFixtureArray = [];

for ($i = 1; $i < 300; $i++) {
    $gamesFixtureArray[] = [
        'id'   => $i,
        'date' => date("Y-m-d H:i:s",time() - $i * 5000),
    ];
}


return $gamesFixtureArray;
