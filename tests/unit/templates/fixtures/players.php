<?php

$playersDataArray = [
    [1, 'Jon', 'Black', 'jonblack@test.com', '44 7700 900011'],
    [2, 'David', 'Wood', 'davidwood@test.com', '44 7700 900012'],
    [3, 'Mike', 'White', 'mikewhite@test.com', '44 7700 900013'],
    [4, 'Adam', 'Smith', 'adamsmith@test.com', '44 7700 900014'],
    [5, 'Jack', 'Brown', 'jackbrown@test.com', '44 7700 900015'],
    [6, 'Martin', 'Cook', 'martincook@test.com', '44 7700 900016'],
    [7, 'James', 'Silva', 'jamessilva@test.com', '44 7700 900017'],
    [8, 'Kirsty', 'Barnes', 'kirstybarnes@test.com', '44 7700 900018'],
    [9, 'John', 'Brzenk', 'johnbrzenk@test.com', '44 7700 900019'],
    [10, 'Muhamed', 'Ali', 'muhamedali@test.com', '44 7700 900020'],
    [11, 'Alex', 'Martin', 'alexmartin@test.com', '44 7700 900021'],
    [12, 'Ted', 'Sedman', 'tedsedman@test.com', '44 7700 900022'],
];


$playersFixtureArray = [];
foreach ($playersDataArray as $player) {
    $playersFixtureArray[] = [
        'id' => $player[0],
        'firstName' => $player[1],
        'lastName' => $player[2],
        'email' => $player[3],
        'phone' => $player[4],
    ];
}

return $playersFixtureArray;
