<?php

use yii\db\Migration;

class m170913_133801_create_tables extends Migration
{
    const PLAYERS_TABLE_NAME      = 'players';
    const GAMES_TABLE_NAME        = 'games';
    const PLAYER_GAMES_TABLE_NAME = 'player_games';

    /**
     *
     */
    public function up()
    {
        $this->createPlayersTable();
        $this->createGamesTable();
        $this->createPlayerGamesTable();
    }

    /**
     *
     */
    public function down()
    {
        $this->dropForeignKey('fk-player-id',self::PLAYER_GAMES_TABLE_NAME);
        $this->dropForeignKey('fk-game-id',self::PLAYER_GAMES_TABLE_NAME);

        $this->dropTable(self::PLAYER_GAMES_TABLE_NAME);
        $this->dropTable(self::GAMES_TABLE_NAME);
        $this->dropTable(self::PLAYERS_TABLE_NAME);
    }

    private function createPlayersTable()
    {
        $this->createTable(self::PLAYERS_TABLE_NAME, [
            'id'        => $this->primaryKey(),
            'firstName' => $this->string(80)->notNull(),
            'lastName'  => $this->string(80)->notNull(),
            'email'     => $this->string(120),
            'phone'     => $this->string(60),
            'startDate' => $this->dateTime(),
            'endDate'   => $this->dateTime(),
        ]);
    }

    private function createGamesTable()
    {
        $this->createTable(self::GAMES_TABLE_NAME, [
            'id'   => $this->primaryKey(),
            'date' => $this->dateTime()
        ]);
    }

    private function createPlayerGamesTable()
    {
        $this->createTable(self::PLAYER_GAMES_TABLE_NAME, [
            'game_id'   => $this->integer(11),
            'player_id' => $this->integer(11),
            'score'     => $this->integer(),
            'result'    => $this->boolean(),
        ]);

        $this->addPrimaryKey('pk-player-game', self::PLAYER_GAMES_TABLE_NAME, ['game_id', 'player_id']);

        $this->addForeignKey(
            'fk-player-id',
            self::PLAYER_GAMES_TABLE_NAME,
            'player_id',
            self::PLAYERS_TABLE_NAME,
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-game-id',
            self::PLAYER_GAMES_TABLE_NAME,
            'game_id',
            self::GAMES_TABLE_NAME,
            'id',
            'CASCADE'
        );
    }
}
