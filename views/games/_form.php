<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Games */
/* @var $playersNameArray array */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="games-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    echo '<label>Date</label>';
    echo DatePicker::widget([
        'name' => 'Games[startDate]',
        'value' => date('Y-m-d H:i:s', strtotime('-1 day')),
        'options' => ['placeholder' => 'Select issue date ...'],
        'pluginOptions' => [
            'format' => 'dd-M-yyyy',
            'todayHighlight' => true
        ]
    ]);
    ?>

    <?php
    echo '<label class="control-label">Winner</label>';
    echo Select2::widget([
        'name' => 'Games[winner]',
        'data' => $playersNameArray,
        'options' => ['placeholder' => 'Select a winner ...'],
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [',', ' '],
            'maximumInputLength' => 80
        ],
    ]);
    ?>

    <?php
    echo '<label class="control-label">Loser</label>';
    echo Select2::widget([
        'name' => 'Games[loser]',
        'data' => $playersNameArray,
        'options' => ['placeholder' => 'Select a loser ...'],
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [',', ' '],
            'maximumInputLength' => 80
        ],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
