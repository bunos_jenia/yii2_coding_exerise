<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PlayersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Leader Board';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="players-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'Average Score',
                'value' => function($model) {
                    return $model['average'];
                },
            ],
            [
                'attribute' => 'Full Name',
                'format' => 'raw',
                'value' => function ($model) {
                    $linkText = $model['player']['firstName'] . ' ' . $model['player']['lastName'];
                    return Html::a($linkText, ['players/view', 'id' => $model['player_id']]);
                },
            ],

        ],
    ]); ?>
</div>
