<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Players */

$this->title = $model->getFullName();
$this->params['breadcrumbs'][] = ['label' => 'Players', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="players-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'firstName',
            'lastName',
            'email:email',
            'phone',
            'startDate',
            'endDate',
        ],
    ]) ?>

    <?= DetailView::widget([
        'model' => $playerStatistics,
        'attributes' => [
            [
                'attribute' => 'Average Score',
                'value' => function ($playerStatistics) {
                    return $playerStatistics->getAverageScore();
                },
            ],
            [
                'attribute' => 'Number of wins',
                'value' => function ($playerStatistics) {
                    return count($playerStatistics->getGames(true));
                },
            ],
            [
                'attribute' => 'Number of losses',
                'value' => function ($playerStatistics) {
                    return count($playerStatistics->getGames(false));
                },
            ],
            [
                'attribute'=>'Highest Score Game Link',
                'format'=>'raw',
                'value' => function ($playerStatistics) {
                    $gameId = $playerStatistics->getHighestScoreGameId();
                    return Html::a('Link', ['games/view', 'id' => $gameId]);
                },
            ],
            [
                'attribute' => 'Highest Score',
                'value' => function ($playerStatistics) {
                    return $playerStatistics->getHighestScore();
                },
            ],
            [
                'attribute' => 'Highest Score Game Result',
                'value' => function ($playerStatistics) {
                    if ($playerStatistics->getHighestScoreGameResult() == 1) {
                        return 'Win';
                    }

                    return 'Lose';
                },
            ],
            [
                'attribute' => 'Opponent Name',
                'value' => function ($playerStatistics) {
                    return $playerStatistics->getHighestScoreGameOpponentName();
                },
            ],
        ],
    ]) ?>

</div>
